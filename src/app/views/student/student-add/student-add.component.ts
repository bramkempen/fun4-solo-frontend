import {Component, OnInit} from '@angular/core';
import {Student} from '../../../classes/student';
import {StudentService} from '../../../services/student.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.scss']
})
export class StudentAddComponent implements OnInit {
  public student: Student;

  constructor(private router: Router, private studentService: StudentService) {
    this.student = new Student();
  }

  ngOnInit() {
  }

  submitForm() {
    console.log(this.student, 'Student');
    this.studentService.save(this.student).subscribe(data => {
      console.log(data, 'Save data return');
      this.router.navigateByUrl('/student/list');
    });
  }

  resetStudent() {
    this.student = new Student();
  }
}
