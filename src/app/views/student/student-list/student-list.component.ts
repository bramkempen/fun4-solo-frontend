import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import {FeedpulseService} from '../../../services/feedpulse.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ServiceEnum} from '../../../enums/serviceEnum';
import {Student} from '../../../classes/student';
import {StudentService} from '../../../services/student.service';


@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent {
  public tableWidget: any;
  public httpError: HttpErrorResponse = null;
  public students: Student[];

  i = 0;
  total = 0;

  constructor(private studentService: StudentService) {
    this.studentService.getAll().subscribe(
      data => {
        console.log(data, 'Data');
        this.students = [];
        this.total = data.length;
        for (let row of data) {
          this.addStudentToList(row);
        }
        console.log(this.students, 'studenten');

        //this.reInitDatatable();
      }, error => {
        this.httpError = error;
        console.error('Couldn\'t connect to the rest server', error);
      }
    );
  }

  private addStudentToList(id: any) {
    this.studentService.getById(id).subscribe(
      data => {
        this.students.push(Student.fromJSON(data));
        this.i++;
        if (this.i === this.total) {
          this.reInitDatatable();
        }
      }
    );
  }

  private reInitDatatable(): void {
    if (this.tableWidget) {
      this.tableWidget.destroy();
      this.tableWidget = null;
    }
    setTimeout(() => this.initDatatable(), 0);
  }

  protected initDatatable(): void {
    const table: any = $('#datatable');
    this.tableWidget = table.DataTable({
      //data: this.incidentsToTableArray(this.incidents),
      select: true
    });
  }

}
