import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StudentRoutingModule} from './student-routing.module';
import {StudentListComponent} from './student-list/student-list.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import {UtilModule} from '../../UitlModule';
import { StudentAddComponent } from './student-add/student-add.component';

@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    UtilModule
  ],
  declarations: [
    StudentListComponent,
    StudentDetailComponent,
    StudentAddComponent
  ]
})
export class StudentModule { }
