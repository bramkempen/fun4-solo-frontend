import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Student} from '../../../classes/student';
import {FeedpulseService} from '../../../services/feedpulse.service';
import {HttpErrorResponse} from '@angular/common/http';
import {StudentService} from '../../../services/student.service';
import {Feedpulse} from '../../../classes/feedpulse';
import * as $ from 'jquery';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss']
})
export class StudentDetailComponent implements OnInit {

  public tableWidget: any;
  public id: number;
  public student: Student = null;
  public httpError: HttpErrorResponse = null;

  constructor(private router: Router, private studentService: StudentService, private route: ActivatedRoute) {
  }

  submitForm() {
    console.log(this.student, 'Student');
    this.studentService.save(this.student).subscribe(data => {
      console.log(data, 'Save data return');
      this.router.navigateByUrl('/student/list');
    });
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        this.getStudentWithId();
      }
    );
  }

  addFeedpulse() {
    var feedpulse = new Feedpulse();
    this.student.feedpulses.push(feedpulse);
    this.studentService.save(this.student).subscribe(data => {
      this.getStudentWithId();
      this.reInitDatatable();
    });
  }

  getStudentWithId() {
    this.studentService.getById(this.id).subscribe(
      data => {
        this.student = Student.fromJSON(data);
        console.log(this.student, 'Student');
        this.reInitDatatable();
      }
    );
  }

  private reInitDatatable(): void {
    if (this.tableWidget) {
      this.tableWidget.destroy();
      this.tableWidget = null;
    }
    setTimeout(() => this.initDatatable(), 0);
  }

  protected initDatatable(): void {
    const table: any = $('#datatable');
    this.tableWidget = table.DataTable({
      //data: this.incidentsToTableArray(this.incidents),
      select: true
    });
  }
}
