import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StudentListComponent} from './student-list/student-list.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';
import {StudentAddComponent} from './student-add/student-add.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Student'
    },
    children: [
      {
        path: '',
        component: StudentListComponent,
        data: {
          title: 'Student list'
        }
      },
      {
        path: 'list',
        component: StudentListComponent,
        data: {
          title: 'Student list'
        }
      },
      {
        path: 'detail/:id',
        component: StudentDetailComponent,
        data: {
          title: 'Student detail'
        }
      },
      {
        path: 'add',
        component: StudentAddComponent,
        data: {
          title: 'Student add'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule {}
