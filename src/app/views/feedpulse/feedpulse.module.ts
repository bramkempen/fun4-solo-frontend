import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FeedpulseRoutingModule} from './feedpulse-routing.module';
import { FeedpulseDetailComponent } from './feedpulse-detail/feedpulse-detail.component';
import {UtilModule} from '../../UitlModule';

@NgModule({
  imports: [
    CommonModule,
    FeedpulseRoutingModule,
    UtilModule
  ],
  declarations: [
    FeedpulseDetailComponent,
  ]
})
export class FeedpulseModule { }
