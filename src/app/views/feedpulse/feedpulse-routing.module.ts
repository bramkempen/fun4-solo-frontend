import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeedpulseDetailComponent} from './feedpulse-detail/feedpulse-detail.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Feedpulse'
    },
    children: [
      {
        path: 'detail/:id',
        component: FeedpulseDetailComponent,
        data: {
          title: 'Feedpulse detail'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedpulseRoutingModule {}
