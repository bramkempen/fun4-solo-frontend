import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FeedpulseService} from '../../../services/feedpulse.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Feedpulse} from '../../../classes/feedpulse';
import * as $ from 'jquery';
import {FormControl} from '@angular/forms';
import {Message} from '../../../classes/message';

@Component({
  selector: 'app-feedpulse-detail',
  templateUrl: './feedpulse-detail.component.html',
  styleUrls: ['./feedpulse-detail.component.scss']
})
export class FeedpulseDetailComponent implements OnInit {

  public id: number;
  public feedpulse: Feedpulse = null;
  public textarea = new FormControl();
  public httpError: HttpErrorResponse = null;

  constructor(private router: Router, private feedpulseService: FeedpulseService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        this.getFeedpulseWithId();
      }
    );
  }

  addFeedpulse() {
    var message = new Message();
    message.content = this.textarea.value;
    this.textarea = new FormControl();

    this.feedpulse.messages.push(message);

    this.feedpulseService.save(this.feedpulse).subscribe(data => {
      console.log(data);
      this.getFeedpulseWithId();
    });
  }

  getFeedpulseWithId() {
    this.feedpulseService.getById(this.id).subscribe(
      data => {
        this.feedpulse = Feedpulse.fromJSON(data);

        for (let row of this.feedpulse.messages) {
          row.created = new Date(row.created);
        }

        console.log(data, 'Data');
        console.log(this.feedpulse, 'Feedpulse');
      }
    );
  }
}
