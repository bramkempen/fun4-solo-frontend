import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  constructor() {}

  username = new FormControl();
  password = new FormControl();

  executeForm() {
    console.log("Submit form");
    console.log(this.username.value);
    console.log(this.password.value);
  }
}
