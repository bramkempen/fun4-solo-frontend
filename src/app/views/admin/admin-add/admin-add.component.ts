import {Component, OnInit} from '@angular/core';
import {Admin} from '../../../classes/admin';
import {AdminService} from '../../../services/admin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.scss']
})
export class AdminAddComponent implements OnInit {
  public admin: Admin;

  constructor(private router: Router, private adminService: AdminService) {
    this.admin = new Admin();
  }

  ngOnInit() {
  }

  submitForm() {
    console.log(this.admin, 'Admin');
    this.adminService.save(this.admin).subscribe(data => {
      console.log(data, 'Save data return');
      this.router.navigateByUrl('/admin/list');
    });
  }

  resetAdmin() {
    this.admin = new Admin();
  }
}
