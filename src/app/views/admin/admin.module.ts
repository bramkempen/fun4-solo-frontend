import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminListComponent} from './admin-list/admin-list.component';
import { AdminDetailComponent } from './admin-detail/admin-detail.component';
import {UtilModule} from '../../UitlModule';
import { AdminAddComponent } from './admin-add/admin-add.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    UtilModule
  ],
  declarations: [
    AdminListComponent,
    AdminDetailComponent,
    AdminAddComponent
  ]
})
export class AdminModule { }
