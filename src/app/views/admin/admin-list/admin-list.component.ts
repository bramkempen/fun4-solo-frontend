import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import {FeedpulseService} from '../../../services/feedpulse.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ServiceEnum} from '../../../enums/serviceEnum';
import {Admin} from '../../../classes/admin';
import {AdminService} from '../../../services/admin.service';


@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss']
})
export class AdminListComponent {
  public tableWidget: any;
  public httpError: HttpErrorResponse = null;
  public admins: Admin[];

  i = 0;
  total = 0;

  constructor(private adminService: AdminService) {
    this.adminService.getAll().subscribe(
      data => {
        console.log(data, 'Data');
        this.admins = [];
        this.total = data.length;
        for (let row of data) {
          this.addAdminToList(row);
        }
        console.log(this.admins, 'adminen');

        //this.reInitDatatable();
      }, error => {
        this.httpError = error;
        console.error('Couldn\'t connect to the rest server', error);
      }
    );
  }

  private addAdminToList(id: any) {
    this.adminService.getById(id).subscribe(
      data => {
        console.log(data, 'Data');
        this.admins.push(Admin.fromJSON(data));
        this.i++;
        if (this.i === this.total) {
          this.reInitDatatable();
        }
      }
    );
  }

  private reInitDatatable(): void {
    if (this.tableWidget) {
      this.tableWidget.destroy();
      this.tableWidget = null;
    }
    setTimeout(() => this.initDatatable(), 0);
  }

  protected initDatatable(): void {
    const table: any = $('#datatable');
    this.tableWidget = table.DataTable({
      //data: this.incidentsToTableArray(this.incidents),
      select: true
    });
  }

}
