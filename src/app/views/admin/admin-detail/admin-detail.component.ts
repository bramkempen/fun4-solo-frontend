import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Admin} from '../../../classes/admin';
import {FeedpulseService} from '../../../services/feedpulse.service';
import {HttpErrorResponse} from '@angular/common/http';
import {AdminService} from '../../../services/admin.service';

@Component({
  selector: 'app-admin-detail',
  templateUrl: './admin-detail.component.html',
  styleUrls: ['./admin-detail.component.scss']
})
export class AdminDetailComponent implements OnInit {

  public id: number;
  public admin: Admin = null;
  public httpError: HttpErrorResponse = null;

  constructor(private router: Router, private adminService: AdminService, private route: ActivatedRoute) {
  }

  submitForm() {
    console.log(this.admin, 'Admin');
    this.adminService.save(this.admin).subscribe(data => {
      console.log(data, 'Save data return');
      this.router.navigateByUrl('/admin/list');
    });
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        this.adminService.getById(this.id).subscribe(
          data => {
            this.admin = Admin.fromJSON(data);
            console.log(this.admin, 'Admin');
          }
        );
      }
    );
  }

}
