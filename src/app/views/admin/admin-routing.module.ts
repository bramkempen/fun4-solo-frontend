import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminListComponent} from './admin-list/admin-list.component';
import {AdminDetailComponent} from './admin-detail/admin-detail.component';
import {AdminAddComponent} from './admin-add/admin-add.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Admin'
    },
    children: [
      {
        path: '',
        component: AdminListComponent,
        data: {
          title: 'Admin list'
        }
      },
      {
        path: 'list',
        component: AdminListComponent,
        data: {
          title: 'Admin list'
        }
      },
      {
        path: 'detail/:id',
        component: AdminDetailComponent,
        data: {
          title: 'Admin detail'
        }
      },
      {
        path: 'add',
        component: AdminAddComponent,
        data: {
          title: 'Admin add'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
