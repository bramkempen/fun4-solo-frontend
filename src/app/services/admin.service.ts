import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GenericService} from './generic.service';
import {Student} from '../classes/student';
import {Admin} from '../classes/admin';

@Injectable()
export class AdminService extends GenericService<Admin> {
  constructor(http: HttpClient) {
    super(http);
    this.domain = 'admin/';
  }
}
