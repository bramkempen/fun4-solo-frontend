import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GenericService} from './generic.service';
import {Student} from '../classes/student';

@Injectable()
export class StudentService extends GenericService<Student> {
  constructor(http: HttpClient) {
    super(http);
    this.domain = 'student/';
  }
}
