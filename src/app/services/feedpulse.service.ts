import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GenericService} from './generic.service';
import {Student} from '../classes/student';
import {Feedpulse} from '../classes/feedpulse';

@Injectable()
export class FeedpulseService extends GenericService<Feedpulse> {
  constructor(http: HttpClient) {
    super(http);
    this.domain = 'feedpulse/';
  }
}
