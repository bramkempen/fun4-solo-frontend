export class Account {
  private _id: number;
  private _email: string;
  private _passHash: string;


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get passHash(): string {
    return this._passHash;
  }

  set passHash(value: string) {
    this._passHash = value;
  }
}
