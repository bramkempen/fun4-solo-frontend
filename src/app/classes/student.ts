import {Account} from './account';
import {Feedpulse} from './feedpulse';

export class Student extends Account {

  private _feedpulses: Feedpulse[];

  static fromJSON(data: any) {
    return Object.assign(new this, data);
  }

  get feedpulses(): Feedpulse[] {
    return this._feedpulses;
  }

  get feedpulsesWithId(): Feedpulse[] {
    var tmp = [];
    for (let feedpulse of this.feedpulses) {
      if (feedpulse.id > 0) {
        tmp.push(feedpulse);
      }
    }
    return tmp;
  }

  set feedpulses(value: Feedpulse[]) {
    this._feedpulses = value;
  }
}
