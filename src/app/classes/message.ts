export class Message {

  private _id: number;
  private _content: string;
  private _rating: string;
  private _created: Date;
  private _updated: Date;

  static fromJSON(data: any) {
    return Object.assign(new this, data);
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }

  get rating(): string {
    return this._rating;
  }

  set rating(value: string) {
    this._rating = value;
  }

  get created(): Date {
    return this._created;
  }

  set created(value: Date) {
    this._created = value;
  }

  get updated(): Date {
    return this._updated;
  }

  set updated(value: Date) {
    this._updated = value;
  }
}
