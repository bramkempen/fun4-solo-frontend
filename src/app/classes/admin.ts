import {Account} from './account';
import {Feedpulse} from './feedpulse';
import {Student} from './student';

export class Admin extends Account {

  private _students: Student[];

  static fromJSON(data: any) {
    return Object.assign(new this, data);
  }


  get students(): Student[] {
    return this._students;
  }

  set students(value: Student[]) {
    this._students = value;
  }
}
